//最简单的饼图
var myChart = echarts.init(document.getElementById("t1"));
var option1 = {
    textStyle: {
        color: '#fff',
        fontSize: 22,
        fontFamily: 'serif',
        fontStyle: 'italic',
    },
    title: {
        text: '测试',
    },
    series: [{
        type:"pie",
        data:[{
            name: '优秀',
            value: 2,
        },{
            name: '及格',
            value: 3,
        },{
            name: '良好',
            value: 4,
        },{
            name: '差',
            value: 1,
        }],
        label: {
            show: true,
            position: 'inside',
        },
    }],
};
myChart.setOption(option1);
//-------------------------
//平滑，多数据
var myChart2 = echarts.init(document.getElementById("t2"),"shine");
var option2 = {
    textStyle: {
        color: 'red',
        fontSize: 22,
        fontFamily: 'serif',
        fontStyle: 'italic',
    },
    title: {
        text: '早餐销量',
        left: 'center',
     }, 
     xAxis: {
        type: 'category',
        data: ['mon','tue','wed','thu','fri','sat','sun'],
        name: '日期',
     },
     yAxis: {
        type: 'value',
        name: '销售量',
        min: 0,
        max: 42,
     },
     legend: {
        top: 'bottom',
     },
     series: [{
        type:'line',
        name: '馒头',
        data: [35,42,28,26,39,25,40],
        smooth: true,
     },{
      type:'line',
      name: '包子',
      data: [9,4,5,12,14,8,11],
      smooth: true,
     },{
      type:'line',
      name: '烧饼',
      data: [17,22,19,21,25,16,28],
      smooth: true,
     }],    
        
};
myChart2.setOption(option2);
//------------------------------------
//设置数据
var myChart3 = echarts.init(document.getElementById("t3"),"mytheme");
var option3 = {
   title: {
      text: '一周内每日作业上交情况',
      left: 'center',
   }, 
   //柱状图比较不同离散数据的差别
   xAxis: {
      type: 'category',
      data: ['mon','tue','wed','thu','fri','sat','sun'],
      name: '日期',
   },
   yAxis: {
      type: 'value',
      name: '每日作业提交数',
      min: 0,
      max: 42,
   },
   series: [{
      type:'bar',
      data: [35,42,18,26,39,5,40],
      //设置标签显示
      label: {
         show: true,
         position: 'top',
      },
   }],    
};
myChart3.setOption(option3);