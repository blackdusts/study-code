//最简单的地图
var myChart = echarts.init(document.getElementById("t1"));
var option1 = {
    title: {
        text: '中国地图',
        left: 'center',
        top: "10%",
    },
    series: [{
        type:"map",
        map: 'china',
    }],
        
};
myChart.setOption(option1);
//-----
//最简单的地图
var myChart2 = echarts.init(document.getElementById("t2"));
var option2 = {
    title: {
        text: '世界地图',
        left: 'center',
        top: "90%",
    },
    series: [{
        type:"map",
        map: 'world',
        //调整位置
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        //根据经纬度调整范围
        boundingCoords: [[-180,90],[180,-90]],
    }],
        
};
myChart2.setOption(option2);
//------------------------
var myChart3 = echarts.init(document.getElementById("t3"));
var option3 = {
    title: {
        text: '中国地图',
        left: 'center',
        top: "5%",
    },
    series: [{
        type:"map",
        map: 'china',
        //支持平移和缩放
        roam: true,
        //选中模式
        selectedMode: 'multiple',
        //鼠标悬停在某个区域时的样式
        emphasis: {
            itemStyle: {
                areaColor: 'red',
                opacity: 0.3,
            },
        },//选中区域的样式设置
        select:{
            itemStyle:{
                areaColor:"green",
                opacity:0.5
            },
        },

    }],
        
};
myChart3.setOption(option3);
//----------------------------------
//------------------------
var myChart4 = echarts.init(document.getElementById("t4"));
var option4 = {
    title: {
        text: '中国地图',
        left: 'center',
        top: "90%",
    },
    series: [{
        type:"map",
        map: 'china',
        //支持平移和缩放
        roam: true,
        //添加数据
        data: [{
            //这个名字是写在地图文件里的，要与其保持一致
            name: '浙江',
            value: 10,
            //区域样式
            itemStyle: {
                areaColor: 'yellow',
            },
            //标签
            label: {
                show: true,
            },
        },{
            name: '广东',
            value: 50,
            //区域样式
            itemStyle: {
                areaColor: 'red',
            },
            //标签
            label: {
                show: true,
            },
        }],

    }],
        
};
myChart4.setOption(option4);
//-----------------------------------
//------------------------
var myChart5 = echarts.init(document.getElementById("t5"));
var option5 = {
    title: {
        text: 'geo地图绘制',
        left: 'center',
        top: "5%",
    },
    //修改背景色
    backgroundColor: 'rgba(1,20,20,0.5)',
    geo: {
        map: 'china',
        roam: true,
        itemStyle: {
            //边框色
            borderColor: 'black',
            //区域色
            areaColor:'gray',
        },
    },      
};
myChart5.setOption(option5);
//------------------------
var myChart6 = echarts.init(document.getElementById("t6"));
var option6 = {
    title: {
        text: 'geo散点地图绘制',
        left: 'center',
        top: "5%",
    },
    geo: {
        map: 'china',
        roam: true,
    },
    //在geo地理坐标系中添加散点图
    series: [{
        type:'scatter',
        coordinateSystem: 'geo',
        //经纬度及其名称
        data: [
                {name:"北京", value:[116.46,39.92,340]},
                {name:"上海", value:[121.48,31.22,100]},
                {name:"西安", value:[108.93,34.27,200]},
                {name:"广州", value:[113.23,23.16,150]}
        ],
        symbolSize: function(value){
            return value[2]/10+15;
        },
        symbol: 'circle',
    }],
        
};
myChart6.setOption(option6);