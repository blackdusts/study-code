//设置数据
var myChart = echarts.init(document.getElementById("t3"));
var option1 = {
   title: {
      text: '一周内每日作业上交情况',
      left: 'center',
   }, 
   //柱状图比较不同离散数据的差别
   xAxis: {
      type: 'category',
      data: ['mon','tue','wed','thu','fri','sat','sun'],
      name: '日期',
   },
   yAxis: {
      type: 'value',
      name: '每日作业提交数',
      min: 0,
      max: 42,
   },
   series: [{
      type:'bar',
      data: [35,42,18,26,39,5,40],
      //设置标签显示
      label: {
         show: true,
         position: 'top',
      },
   }],    
};
myChart.setOption(option1);