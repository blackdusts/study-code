//设置多组数据
var myChart = echarts.init(document.getElementById("t4"));
var option1 = {
   title: {
      text: '一周内每日作业上交情况(多组数据)',
      left: 'center',
   }, 
   //柱状图比较不同离散数据的差别
   xAxis: {
      type: 'category',
      data: ['mon','tue','wed','thu','fri','sat','sun'],
      name: '日期',
   },
   yAxis: {
      type: 'value',
      name: '每日作业提交数',
      min: 0,
      max: 42,
   },
   //设置图例
   legend: {
      top: 'bottom',
   },
   series: [{
      type:'bar',
      name: '1班',
      data: [35,42,18,26,39,5,40],
      //设置标签显示
      label: {
         show: true,
         position: 'top',
      },
   },{
      type:'bar',
      name: '2班',
      data: [39,41,25,19,35,21,14],
      //设置标签显示
      label: {
         show: true,
         position: 'top',
      },
   },{
      type:'bar',
      name: '3班',
      data: [31,33,40,38,31,16,28],
      //设置标签显示
      label: {
         show: true,
         position: 'top',
      },
   }],    
};
myChart.setOption(option1);