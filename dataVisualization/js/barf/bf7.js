//
var myChart7 = echarts.init(document.getElementById("t7"));
var option7 = {
   xAxis: {
      type: 'category',
      axisLine: {
         show: false,
      },
   },
   yAxis: {
      axisLine: {
         show: false,
      },
      axisLabel: {
         show: false,
      },
      splitLine: {
         show: false,
      },
   },
   // series: [{
   //    type:'bar',
   //    data: [5,1,2,3,4],
   // }],
   series: [{
      type:'pictorialBar',
      data: [4,32,16],
      symbol: 'roundRect',
   }],
};
myChart7.setOption(option7);