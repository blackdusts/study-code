//平滑，多数据
var myChart = echarts.init(document.getElementById("t6"));
var option1 = {
    title: {
        text: '早餐销量(多线型)',
        left: 'center',
     }, 
     xAxis: {
        type: 'category',
        data: ['mon','tue','wed','thu','fri','sat','sun'],
        name: '日期',
     },
     yAxis: {
        type: 'value',
        name: '销售量',
        min: 0,
        max: 42,
     },
     legend: {
        top: 'bottom',
     },
     series: [{
        type:'line',
        name: '馒头',
        data: [35,42,28,26,39,25,40],
        symbol: 'circle',
        symbolSize: 20,
        smooth: true,
     },{
      type:'line',
      name: '包子',
      data: [9,4,5,12,14,8,11],
      symbol: 'triangle',
      symbolSize: 20,
      smooth: true,
     },{
      type:'line',
      name: '烧饼',
      data: [17,22,19,21,25,16,28],
      symbol: 'diamond',
      symbolSize: 20,
      smooth: true,
     }],    
        
};
myChart.setOption(option1);