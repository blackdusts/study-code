//最简单的饼图
var myChart = echarts.init(document.getElementById("t1"));
var option1 = {
    title: {
        text: '烧饼销量',
        left: 'center',
     }, 
     xAxis: {
        type: 'category',
        data: ['mon','tue','wed','thu','fri','sat','sun'],
        name: '日期',
     },
     yAxis: {
        type: 'value',
        name: '销售量',
        min: 0,
        max: 42,
     },
     series: [{
        type:'line',
        data: [35,42,18,26,39,5,40],
        //设置标签显示
        label: {
           show: true,
           position: 'top',
        },
     }],          
};
myChart.setOption(option1);