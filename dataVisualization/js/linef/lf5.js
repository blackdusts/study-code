//平滑，多数据
var myChart = echarts.init(document.getElementById("t5"));
var option1 = {
    title: {
        text: '早餐销量(多图混合)',
        left: 'center',
     }, 
     xAxis: {
        type: 'category',
        data: ['mon','tue','wed','thu','fri','sat','sun'],
        name: '日期',
     },
     yAxis: {
        type: 'value',
        name: '销售量',
        min: 0,
        max: 42,
     },
     legend: {
        top: 'bottom',
     },
     series: [{
        type:'scatter',
        name: '馒头',
        data: [35,42,28,26,39,25,40],
     },{
      type:'bar',
      name: '包子',
      data: [9,4,5,12,14,8,11],
     },{
      type:'line',
      name: '烧饼',
      data: [17,22,19,21,25,16,28],
      smooth: true,
     }],    
        
};
myChart.setOption(option1);