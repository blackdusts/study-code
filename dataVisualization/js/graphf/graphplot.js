//最简单的关系图
var myChart1 = echarts.init(document.getElementById("t1"));
var option1 = {
    title:{
        text: '固定定位',
        left: 'center',
    },
    series: [{
        type:"graph",
        data:[{
            //节点的名字
            name:"小明",
            //节点的相对位置，一定要有，没有就会报错
            x:10,
            y:50,
        },{
            name:"老王",
            x:90,
            y:50,
        },{
            name: '小红',
            x:50,
            y:100,
        }],
        links: [{
          //有关系的两者
          source: '小明',
          target: '小红',
        },{
            source: '小红',
            target: '老王',
          },{
            source: '老王',
            target: '小明',
          }],
    }],      
};
myChart1.setOption(option1);
//------------------------------
//最简单的关系图
var myChart2 = echarts.init(document.getElementById("t2"));
var option2 = {
    title: {
        text: '样式设置',
        left: 'center',
    },
    series: [{
        type:"graph",
        data:[{
            //节点的名字
            name:"小明",
            //节点的相对位置，一定要有，没有就会报错
            x:10,
            y:50,
        },{
            name:"老王",
            x:90,
            y:50,
        },{
            name: '小红',
            x:50,
            y:100,
        }],
        //节点标签信息
        label: {
            show: true,
            position: 'bottom',
            fontSize: 18,
            color: 'black',
        },
        //设置节点大小
        symbolSize: 40,
        symbol: 'triangle',
        // symbol: 'rect',
        itemStyle: {
            color: 'red',
            borderColor: 'black',
            borderWidth: 5,
            opacity: 0.5,
        },
        links: [{
          //有关系的两者
          source: '小明',
          target: '小红',
          value: 10,
          //标签样式设置
          label: {
              color: 'red',
              fontSize: 30,
              formatter: '喜欢',
              show: true,
          },
          //文本样式设置
          lineStyle: {
              width: 5,
              color: 'green',
          },
        },{
            source: '小红',
            target: '老王',
          },{
            source: '老王',
            target: '小明',
          }],
    }],    
};
myChart2.setOption(option2);
//----------------------------
var myChart3 = echarts.init(document.getElementById("t3"));
var option3 = {
    title: {
        text: '力引导布局',
        left: 'center',
    },
    series: [{
        type:"graph",
        //力引导布局
        layout: 'force',
        force: {
            //设置节点初始间距
            repulsion: 3000,
        },
        //允许鼠标拖拽缩放和整体平移
        roam: true,
        //单个节点拖拽，只在力引导布局下有用
        draggable: true,
        symbolSize: 50,
        label: {
            show: true,
            position: 'top',
        },
        symbol: 'image://https://image.niuzhan.com/uploads/online_edit_pic/20210108/1610090204123703.jpg',
        data:[{
            //节点的名字
            name:"小明",
        },{
            name:"老王",
        },{
            name: '小红',
        }],
        links: [{
          //有关系的两者
          source: '小明',
          target: '小红',
        },{
            source: '小红',
            target: '老王',
          },{
            source: '老王',
            target: '小明',
          }],
    }],      
};
myChart3.setOption(option3);
//-------------------------------------------------
var myChart4 = echarts.init(document.getElementById("t4"));
var option4 = {
    title: {
        text: '力引导布局',
        left: 'center',
    },
    series: [{
        type:"graph",
        //力引导布局
        layout: 'force',
        force: {
            //设置节点初始间距
            repulsion: 1000,
        },
        //允许鼠标拖拽缩放和整体平移
        roam: true,
        //单个节点拖拽，只在力引导布局下有用
        draggable: true,
        symbolSize: 50,
        label: {
            show: true,
            position: 'top',
        },
        data:[{
            //节点的名字
            name:"小明",
            symbol: 'image://../../img/tutu.jpg',
        },{
            name:"老王",
            symbol: 'image://../../img/laowang.jpeg',
        },{
            name: '小红',
            symbol: 'image://../../img/xiaomei.jpeg',
        }],
        links: [{
          //有关系的两者
          source: '小明',
          target: '小红',
        },{
            source: '小红',
            target: '老王',
          },{
            source: '老王',
            target: '小明',
          }],
    }],      
};
myChart4.setOption(option4);
//-----------------------
var myChart5 = echarts.init(document.getElementById("t5"));
var option5 = {
    title: {
        text: '力引导布局样式设置',
        left: 'center',
    },
    series: [{
        type:"graph",
        //力引导布局
        layout: 'force',
        force: {
            //设置节点初始间距
            repulsion: 300,
        },
        //允许鼠标拖拽缩放和整体平移
        roam: true,
        //单个节点拖拽，只在力引导布局下有用
        draggable: true,
        symbolSize: 30,
        label: {
            show: true,
            position: 'inside',
        },
        //鼠标悬停在节点时，只显示直接相关的节点，其他非相关的节点不显示
        focusNodeAdjacency: true,
        //节点类别设置
        categories: [
            {
                name: '学生',
                itemStyle: {
                    color: 'green',
                },
            },{
                name: '民工',
                itemStyle: {
                    color: 'blue',
                },
            },{
                name: '地主',
                itemStyle: {
                    color: 'red',
                },
            }
        ],
        data:[{
            //节点的名字
            name:"小明",
            category: 1,
        },{
            name:"老王",
            category: 2,
        },{
            name: '小红',
            category: 0,
        },{
            name: '小赵',
            category: 1,
        },{
            name: '小钱',
            category: 2,
        },{
            name: '小孙',
            category: 0,
        },{
            name: '小李',
            category: 1,
        },{
            name: '小周',
            category: 2,
        },{
            name: '小吴',
            category: 0,
        }],
        links: [{
          //有关系的两者
          source: '小明',
          target: '小红',
        },{
            source: '小红',
            target: '老王',
          },{
            source: '老王',
            target: '小明',
          },{
            source: 1,
            target: 3,
          },{
            source: 3,
            target: 4,
          },{
            source: 2,
            target: 5,
          },{
            source: 4,
            target: 6,
          },{
            source: 3,
            target: 7,
          },{
            source: 1,
            target: 8,
          },{
            source: 1,
            target: 9,
          }],
    }],      
};
myChart5.setOption(option5);
//-----------------------------
var myChart6 = echarts.init(document.getElementById("t6"));
var option6 = {
    title: {
        text: '环形布局',
        left: 'center',
    },
    series: [{
        type:"graph",
        //环形引导布局
        layout: 'circular',
        //允许鼠标拖拽缩放和整体平移
        roam: true,
        symbolSize: 50,
        label: {
            show: true,
            position: 'inside',
        },
        data:[{
            name:"小明",
        },{
            name:"老王",
        },{
            name: '小红',
        },{
            name: '小王',
        },{
            name: '小刘',
        },{
            name: '小李',
        },{
            name: '小赵',
        }],
        links: [{
          //有关系的两者
          source: '小明',
          target: '小红',
        },{
            source: '小红',
            target: '老王',
          }],
    }],      
};
myChart6.setOption(option6);