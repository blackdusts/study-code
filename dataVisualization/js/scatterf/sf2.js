//生成随机数字
var x,y;
var dataTest = [];
for(var i=0;i<200;i++){
   x=100*Math.random();
   y=100*Math.random();
   dataTest.push([x,y]);
}
var myChart = echarts.init(document.getElementById("t2"));
var option1 = {
    title: {
        text: '石子落地位置',
        left: 'center',
     }, 
     xAxis: {},
     yAxis: {},
     series: [{
        type:'scatter',
        data: dataTest,
        symbolSize: 5,
     }],    
        
};
myChart.setOption(option1);