//生成随机数字
var x,y;
var dataTest = [];
for(var i=0;i<200;i++){
   x=100*Math.random();
   y=100*Math.random();
   dataTest.push([x,y]);
}
var myChart = echarts.init(document.getElementById("t4"));
var option1 = {
    title: {
        text: '石子落地位置',
        subtext: '添加滑动条区域缩放组件',
        left: 'center',
     }, 
     xAxis: {},
     yAxis: {},
     //缩放组件不同的组合效果不同
     dataZoom: [{
        type:'slider',
        //设置缩放的轴
        yAxisIndex: 0, 
        backgroundColor: 'green', 
     },{
        type:'slider',
        xAxisIndex: 0,
        dataBackground: {
           areaStyle: {
              color: 'red',
           },
           lineStyle: {
              color: 'brown',
           },
        },
     },{
      type:'inside',
      xAxisIndex: 0,
     },{
      type:'inside',
      yAxisIndex: 0,
     }],
     series: [{
        type:'scatter',
        data: dataTest,
        symbolSize: 5,
     }],    
        
};
myChart.setOption(option1);