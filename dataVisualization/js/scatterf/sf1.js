//最简单的散点图
var myChart = echarts.init(document.getElementById("t1"));
var option1 = {
    title: {
        text: '石子落地位置',
        left: 'center',
     }, 
     xAxis: {},
     yAxis: {},
     series: [{
        type:'scatter',
        data: [[1,4],[9,6],[5,5],[4,8],[3,2],[7,3],[10,4]],
     }],    
        
};
myChart.setOption(option1);