//饼图
var myChart1 = echarts.init(document.getElementById("t1"));
var option1 = {
    series: [{
        type:'pie',
        data:[
            { name: 'C/C++',value: 3,},
            { name: 'JavaScript', value: 4,},
            { name: 'Python', value: 5,}
        ],
        label: {
            show: true,
            color: 'black',
            position: 'inside',
            fontSize: 18,
        },
    }],
};
myChart1.setOption(option1);
//柱状图
var myChart2 = echarts.init(document.getElementById("t2"));
var option2 = {
    xAxis: {
        type: 'category',
        data: ['语文','数学','英语','物理'],
    },
    yAxis: {
        splitLine: {
            show: false,
        },
        axisLabel: {
            show: false,
        },
    },
    series: [{
        type:'bar',
        data:[80,99,60,95],
        label: {
            show: true,
            color: 'black',
            position: 'inside',
            fontSize: 18,
        },
    }],
};
myChart2.setOption(option2);
//散点图
var myChart3 = echarts.init(document.getElementById("t3"));
var option3 = {
    xAxis: {
        type: 'category',
        data: ['3月','6月','9月','12月'],
    },
    yAxis: {
        splitLine: {
            show: false,
        },
        axisLabel: {
            show: false,
        },
    },
    series: [{
        type:'scatter',
        data:[75,63,86,34],
        symbolSize: 15,
        symbol: 'triangle',
    }],
};
myChart3.setOption(option3);

var myChart5 = echarts.init(document.getElementById("t5"));
var option5 = {
    xAxis: {
        type: 'category',
        data: ['3月','6月','9月','12月'],
    },
    yAxis: {
        splitLine: {
            show: false,
        },
        axisLabel: {
            show: false,
        },
    },
    series: [{
        type:'line',
        data:[15,23,16,34],
        symbolSize: 15,
        smooth: true,
    }],
};
myChart5.setOption(option5);

//散点图
var myChart4 = echarts.init(document.getElementById("t4"));
var option4 = {
    title: {
        text: '散点地图',
        left: 'center',
        top: "5%",
        textStyle: {
            color: 'white',
        },
    },
    geo: {
        map: 'china',
        roam: true,
    },
    //在geo地理坐标系中添加散点图
    series: [{
        type:'effectScatter',
        coordinateSystem: 'geo',
        //经纬度及其名称
        data: [
                {name:"北京", value:[116.46,39.92,340]},
                {name:"上海", value:[121.48,31.22,100]},
                {name:"西安", value:[108.93,34.27,200]},
                {name:"广州", value:[113.23,23.16,150]}
        ],
        symbolSize: function(value){
            return value[2]/10+15;
        },
        symbol: 'circle',
    }],
};
myChart4.setOption(option4);
var myChart6 = echarts.init(document.getElementById("t6"));
var option6 = {
    title: {
        text: '秒表',
        left: 'left',
        top: 'middle',
    },
    series: [
        {
            name: '秒',
            type: 'gauge', 
            radius: '90%',
            min: 0,
            max: 60,
            splitNumber: 12,
            startAngle: 90,
            endAngle: -270,
            data: [{
                value: 1,name:""
            }],
            detail: {
                show: false,
            },
            //解决数字0和60的重叠问题
            axisLabel: {
                formatter:function(value){
                    if(value==0){
                        return '';
                    }
                    return value;
                },
            },
        },
    ]
};
// 使用刚指定的配置项和数据显示图表。
myChart6.setOption(option6);
function asecondAgo(){
    var temp = option6.series[0].data[0].value;
    temp = (temp+1)%60;
    // console.log(temp);
    option6.series[0].data[0].value=temp;
    option6.series[0].animation = (temp!=0) ;
    myChart6.setOption(option6);
}
setInterval(function(){asecondAgo();},1000);

var myChart7 = echarts.init(document.getElementById("t7"));
var option7 = {
    baseOption:{
        timeline:{
            axisType:'category',
            autoPlay:'true',
            //变化间隔
            playInterval: 1000,
            //三组数据
            data:['2020','2021','2022'],
        },
        title:{
            text:'成绩变化',
            left:'center',
        },
        xAxis:{
            type:'category',
            data:["小明","小美","老王"],
        },
        yAxis:{
            name:'成绩',
            min:60,
            max:100,
        },
    },
    options:[{
        series:[{
            type:'bar',
            data: [75,68,83],
        }]
    },{
        series:[{
            type:'bar',
            data: [72,70,88],
        }]
    },{
        series:[{
            type:'bar',
            data: [83,72,90],
        }]
    }],
        
};
myChart7.setOption(option7);
//-------------------------------------------------
var myChart8 = echarts.init(document.getElementById("t8"));
var option8 = {
    
    series: [{
        type:"graph",
        //力引导布局
        layout: 'force',
        force: {
            //设置节点初始间距
            repulsion: 1000,
        },
        roam: true,
        draggable: true,
        symbolSize: 50,
        label: {
            show: true,
            position: 'top',
        },
        data:[{
            //节点的名字
            name:"小明",
            symbol: 'image://../../img/tutu.jpg',
        },{
            name:"老王",
            symbol: 'image://../../img/laowang.jpeg',
        },{
            name: '小红',
            symbol: 'image://../../img/xiaomei.jpeg',
        }],
        links: [{
          //有关系的两者
          source: '小明',
          target: '小红',
        },{
            source: '小红',
            target: '老王',
          },{
            source: '老王',
            target: '小明',
          }],
    }],      
};
myChart8.setOption(option8);