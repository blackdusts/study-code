//--------------------------------------
var myChart5 = echarts.init(document.getElementById("t5"));
var option5 = {
    grid: {
        bottom: '10%',
    },
    xAxis: {
        type: 'category',
        //-------------------------------
        //X轴数据及其样式设置
        data: ['季度1','季度2','季度3','季度4'],
    },
    yAxis: {
         //显示位置
         position: 'right',
         //样式设置
         axisLabel: {
             color: 'green',
             fontSize: 10,
         },
         //------------------------------
         //X轴及其样式设置
         axisLine: {
             show: true,
             lineStyle: {
                 color: 'orange',
                 width: 5,
                //  透明度
                 opacity: 0.5,
                //  阴影效果
                 shadowBlur: 5,
                //  线形
                //  type: 'dashed',
 
             },
         },
         //------------------------------
         //分割线及其属性
         axisTick: {
             // show: false,
             length: 9,
             lineStyle: {
                 color: 'green',
                 width: 10,
             },
         },
         //-----------------------------
         //轴线提示器
         axisPointer: {
             show: true,
             label: {
                 color: 'yellow',
             },
         },
         //------------------------------
         //轴名称及其样式设置
         name: '价值',
         //name显示位置
         nameLocation: 'end',
         //与轴之间的距离
         nameGap: 10,
         //文本旋转角度
         nameRotate: 0,
         //文本样式设置
         nameTextStyle: {
             color: 'red',
             fontFamily: 'monospace',
         },
         //-------------------------------
         //数据背景
         splitArea: {
             show: true,
             areaStyle: {
                 color: ['red','green','yellow'],
                 opacity: 0.5,
             },
         },
         //---------------------------------
         //分割线
         splitLine: {
             show: true,
             lineStyle: {
                 color: ['black','red','green'],
                 width: 5,
             },
         },
    },
    series: {
        type:'line',
        data: [5,3,9,2],
    },
};
myChart5.setOption(option5);
//--------------------------------
//无坐标轴的数据
var myChart6 = echarts.init(document.getElementById("t6"));
var option6 = {
    xAxis: {
        type: 'category',
        axisLine: {
            show: false,
        },
    },
    yAxis: {
        axisLabel: {
            show: false,
        },
        splitLine: {
            show: false,
        },
    }, 
    title: {
        text: '无坐标轴',
    },
    series: [{
        type:'line',
        data:[5,6,8,3,4],
        smooth: true,
    },{
        type:'line',
        data:[2,5,3,1,2],
        smooth: true,
    }],   
};
myChart6.setOption(option6);