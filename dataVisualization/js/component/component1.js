//最简单的坐标系
var myChart1 = echarts.init(document.getElementById("t1"));
var option1 = {
    xAxis: {},
    yAxis: {},    
};
myChart1.setOption(option1);
//------------------------------------
var myChart2 = echarts.init(document.getElementById("t2"));
var option2 = {
    grid: [
        {left: '5%',right: '50%',bottom: '55%', top: '5%',},
        {left: '55%',right: '5%',bottom: '5%', top: '50%',},
    ],
    xAxis: [
        {gridIndex: 0,},
        {gridIndex: 1,}
    ],
    yAxis: [
        {gridIndex: 0,},
        {gridIndex: 1,}
    ],
};
myChart2.setOption(option2);
//---------------------------------------
var myChart3 = echarts.init(document.getElementById("t3"));
var option3 = {
grid: [
    {
        left: '5%',        right: '50%',        bottom: '55%',        top: '5%',
    },{
        left: '10%',        right: '50%',        bottom: '5%',        top: '55%',
    }, {
        left: '55%',        right: '5%',        bottom: '50%',        top: '5%',
    },{
        left: '55%',        right: '5%',        bottom: '5%',        top: '55%',
    }
],
    xAxis: [{ gridIndex: 0,  min: 1,   max: 100,    },
            {gridIndex: 1,},{gridIndex: 2, },{gridIndex: 3,}],
    yAxis: [{ gridIndex: 0,},
            { gridIndex: 1, min: 1,max: 100, },
            { gridIndex: 2, },{ gridIndex: 3,}],
};
myChart3.setOption(option3);
