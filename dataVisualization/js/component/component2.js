//--------------------------------------
var myChart4 = echarts.init(document.getElementById("t4"));
var option4 = {
    grid: {
        bottom: '10%',
    },
    xAxis: {
        type: 'category',
        //-------------------------------
        //X轴数据及其样式设置
        data: ['季度1','季度2','季度3','季度4'],
        //显示位置
        position: 'top',
        //样式设置
        axisLabel: {
            color: 'green',
            fontSize: 10,
            //文本太长竖着放
            formatter: function(value){
                return value.split("").join("\n");
            },
        },
        //------------------------------
        //X轴及其样式设置
        axisLine: {
            // show: false,
            lineStyle: {
                color: 'orange',
                // width: 5,
                //透明度
                // opacity: 0.5,
                //阴影效果
                // shadowBlur: 10,
                //线形
                // type: 'dashed',

            },
        },
        //------------------------------
        //分割线及其属性
        axisTick: {
            // show: false,
            length: 9,
            lineStyle: {
                color: 'green',
                width: 10,
            },
        },
        //-----------------------------
        //轴线提示器
        axisPointer: {
            show: true,
            label: {
                color: 'yellow',
            },
        },
        //------------------------------
        //轴名称及其样式设置
        name: '时间',
        //name显示位置
        nameLocation: 'start',
        //与轴之间的距离
        nameGap: 25,
        //文本旋转角度
        nameRotate: 90,
        //文本样式设置
        nameTextStyle: {
            color: 'red',
            fontFamily: 'monospace',
        },
        //-------------------------------
        //数据背景
        splitArea: {
            show: true,
            areaStyle: {
                color: ['red','green','yellow'],
                opacity: 0.5,
            },
        },
        //---------------------------------
        //分割线
        splitLine: {
            show: true,
            lineStyle: {
                color: ['black','red','green'],
                width: 5,
            },
        },
    },
    yAxis: {

    },
    series: {
        type:'line',
        data: [5,3,9,2],
    },
};
myChart4.setOption(option4);