//获取本地时间
// var now = new Date();
// var hour = now.getHours();
// var minute = now.getMinutes();
// var second = now.getSeconds();
// console.log(hour,minute,second)
//仪表盘
var myChart3 = echarts.init(document.getElementById("t3"));
var option3 = {
    title: {
        text: '秒表',
        left: 'left',
        top: 'middle',
    },
    series: [
        {
            name: '秒',
            type: 'gauge', 
            radius: '90%',
            min: 0,
            max: 60,
            splitNumber: 12,
            startAngle: 90,
            endAngle: -270,
            data: [{
                value: 1,name:""
            }],
            detail: {
                show: false,
            },
            //解决数字0和60的重叠问题
            axisLabel: {
                formatter:function(value){
                    if(value==0){
                        return '';
                    }
                    return value;
                },
            },
            // animationDurationUpdate: 300,
            // animationEasingUpdate: 'cubicOut',
        },
    ]
};
// 使用刚指定的配置项和数据显示图表。
myChart3.setOption(option3);
function asecondAgo(){
    var temp = option3.series[0].data[0].value;
    temp = (temp+1)%60;
    console.log(temp);
    option3.series[0].data[0].value=temp;
    //因为0和12在一个位置上，从11到12就是从11到0，这样会变成逆时针的动画
    //所以在0的时候取消动画，就避免了逆时针
    option3.series[0].animation = (temp!=0) ;
    myChart3.setOption(option3);
}
setInterval(function(){asecondAgo();},1000);