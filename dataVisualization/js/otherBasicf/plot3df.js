//最简单的坐标系
var myChart1 = echarts.init(document.getElementById("t1"));
var option1 = {
    grid3D: {},
    xAxis3D: {},
    yAxis3D: {},
    zAxis3D: {},    
};
myChart1.setOption(option1);
//-----------------------
//最简单的坐标系
var dataX,dataY,dataZ;
var dataTest=[];
for(var i=0;i<100;i++){
  dataX=Math.floor(100*Math.random());
  dataY=Math.floor(100*Math.random());
  dataZ=Math.floor(100*Math.random());
  dataTest[i]=[dataX,dataY,dataZ];
}
var myChart2 = echarts.init(document.getElementById("t2"));
var option2 = {
    grid3D: {},
    xAxis3D: {},
    yAxis3D: {},
    zAxis3D: {}, 
    series: [{
        type:'scatter3D',
        data:dataTest,
        //根据数值调整大小
        symbolSize: function(value){
            return (value[0]/15+value[1]/10+value[2]/20) +5;
        },
        itemStyle: {
            //根据数值调整颜色
            color: function(value){
                // console.log(value);
                var v1=String(Math.round(value.data[0]/100*255));
                var v2=String(Math.round(value.data[1]/100*255));
                var v3=String(Math.round(value.data[2]/100*255));
                return 'rgb('+ v1 +','+v2+','+ v3 +')';
            },
        },
    }],   
};
myChart2.setOption(option2);
//---------------------------------------
var myChart3 = echarts.init(document.getElementById("t3"));
var option3 = {
    grid3D: {},
    xAxis3D: {},
    yAxis3D: {},
    zAxis3D: {}, 
    visualMap: [{
        show:true,
        min:1,
        max:100,
        calculable:true,
        inRange:{
          color: ['black', 'red','green']
        },
      }],
    series: [{
        type:'bar3D',
        data:dataTest,
    }],   
};
myChart3.setOption(option3);
//先用数学函数生成一点数字；
var dataTest2=[];
for(var i=0;i<720;i=i+1){
// for(var i=0;i<25;i=i+0.001){
//  var dataX=(1 + 0.25 * Math.cos(75 * i)) * Math.cos(i);
//  var dataY= (1 + 0.25 * Math.cos(75 * i)) * Math.sin(i);
//  var dataZ=i + 2.0 * Math.sin(75 * i);
    var dataX=Math.sin(i) * Math.cos(i);
    var dataY=Math.sin(i) * Math.sin(i);
    var dataZ=Math.cos(i);
  //注意了，下面的写法要改，应为i不是整数了，所以不能当下标了
 dataTest2.push([dataX,dataY,dataZ]);
}
//画图配置
var myChart4 = echarts.init(document.getElementById("t4"));
var option4 = {
  grid3D: [{}],
  xAxis3D: [{}],
  yAxis3D: [{}],
  zAxis3D: [{}],
  visualMap: [{
    show:true,
    min:1,
    max:25,
    calculable:true,
    inRange:{
      color: ['#313695', '#4575b4', '#74add1', '#abd9e9', '#e0f3f8', '#ffffbf', '#fee090', '#fdae61', '#f46d43', '#d73027', '#a50026'],
    },
  }],
  series: [
    {
      //3D折线图
      type:'line3D',
    //   type:'surface',
      data:dataTest2,
    }
  ],
};
myChart4.setOption(option4);
//---------------------------------------
var myChart5 = echarts.init(document.getElementById("t5"));
var option5 = {
  grid3D: [{}],
  xAxis3D: [{}],
  yAxis3D: [{}],
  zAxis3D: [{}],
  visualMap: [{
    show:true,
    min:1,
    max:25,
    calculable:true,
    inRange:{
      color: ['#313695', '#4575b4', '#74add1', '#abd9e9', '#e0f3f8', '#ffffbf', '#fee090', '#fdae61', '#f46d43', '#d73027', '#a50026'],
    },
  }],
  series: [
    {
      //3D折线图
      type:'surface',
      equation: {
        x: {
            step: 0.1,
            min: -3,
            max: 3,
            },
        y: {
            step: 0.1,
            min: -3,
            max: 3,
            },
        z: function (x, y) {
            return (x * x + y * y);
            }
        }
    },
  ],
};
myChart5.setOption(option5);
//------------------------------
//---------------------------------------
var myChart6 = echarts.init(document.getElementById("t6"));
var option6 = {
  grid3D: [{}],
  xAxis3D: [{}],
  yAxis3D: [{}],
  zAxis3D: [{}],
  series: [
    {
      //3D折线图
      type:'surface',
      //设置采用参数方程
      parametric: true,
      //参数方程
      parametricEquation: {
          u: {
              min: -Math.PI,
              max: Math.PI,
              step: Math.PI / 20
          },
          v: {
              min: 0,
              max: Math.PI,
              step: Math.PI / 20
          },
          x: function (u, v) {
              return Math.sin(v) * Math.sin(u);
          },
          y: function (u, v) {
              return Math.sin(v) * Math.cos(u);
          },
          z: function (u, v) {
              return Math.cos(v);
          }
      }, 
    },
  ],
};
myChart6.setOption(option6);