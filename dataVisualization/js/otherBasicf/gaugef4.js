//仪表盘的复杂设置
//内外两个表盘
//多组数据
//文本设置
var myChart = echarts.init(document.getElementById("t4"));
var option1 = {
    title:{
        text: '多数据多表盘设置',
        top: 'bottom',
        left: 'left',
    },
    series: [{
        type:'gauge',
        min: 0,
        max: 200,
        //仪表盘半径设置
        radius:'90%',
        //仪表刻度的启止角度
        startAngle: 270,
        endAngle: -30,
        data:[{
            value: 80,
            name: '当前速度',
            //设置当前数据value详情
            detail:{
                show:true,
                formatter:'{value} km/h',
                fontSize:10,
                offsetCenter:['-20%','40%']
            },
            title:{
                //设置标签name的位置
                offsetCenter:['-20%','30%'],
                fontSize:10,
            }
        },{
            value: 40,
            name: '以前速度',
            //设置当前数据value详情
            detail:{
                show:true,
                formatter:'{value} km/h',
                fontSize:10,
                offsetCenter:['20%','40%']
            },
            title:{
                //设置标签name的位置
                offsetCenter:['20%','30%'],
                fontSize:10,
            }
        }]
    },{
        type:'gauge',
        min: 0,
        max: 200,
        //仪表盘半径设置
        radius:'99%',
        //仪表刻度的启止角度
        startAngle: 270,
        endAngle: -90,
        data:[{
            value: 80,
            name: '当前速度',
            title:{
                show:false,
            },
            detail:{
                show:false,
            }
        }],
        //  仪表盘轴线设置
        axisLine:{
            lineStyle:{
                color:[
                    [0.3, 'red'], // 0~30% 红轴
                    [0.6, 'green'], // 30~60% 绿轴
                    [0.9, 'blue'], // 60~90% 蓝轴
                    [1,'black']
                ],
            },
        },
        pointer: {
            show: false,
        },
        axisLabel:{
            show:false,
        }
    }],
        
};
myChart.setOption(option1);