$.get("data/test_data.json",function(data){
    // console.log(data);
    var myChart = echarts.init(document.getElementById("t1"));
    var option={
        series : [
            {        
                type: 'pie',    
                data:data.data_pie,
            }
        ]
    };
    myChart.setOption(option);
})