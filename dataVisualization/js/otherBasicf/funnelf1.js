//基础雷达图
var myChart = echarts.init(document.getElementById("t1"));
var option1 = {
    title: {
        text: '马斯洛需求层次(漏斗图)',
    },
    series: [{
        type:'funnel',
         //金字塔排序
        sort: 'ascending',
        data:[{
            value: 5,
            name: '生理需求',
        },{
            value: 4,
            name: '安全需求',
        },{
            value: 3,
            name: '爱与归属',
        },{
            value: 2,
            name: '尊重需求',
        },{
            value: 1,
            name: '自我实现',
        }],
    }],
        
};
myChart.setOption(option1);