//仪表盘的简单设置
var myChart = echarts.init(document.getElementById("t2"));
var option1 = {
    title:{
        text: '仪表盘的简单设置',
        top: 'bottom',
        left: 'center',
    },
    series: [{
        type:'gauge',
        min: 0,
        max: 200,
        //仪表盘半径设置
        radius:'90%',
        data:[{
            value: 80,
            name: '当前速度',
            //仪表盘标题设置
            title:{
                // show:false,
                show:true,
                color:'red',
                backgroundColor:'yellow',
            },
            //设置当前数据详情
            detail:{
                show:true,
                formatter:'{value} km/h',
                fontSize:15,
            },
        }],
        //  仪表盘轴线设置
        axisLine:{
            show:true,
            lineStyle:{
                color:[
                    [0.3, 'red'], // 0~30% 红轴
                    [0.6, 'green'], // 30~60% 绿轴
                    [0.9, 'blue'], // 60~90% 蓝轴
                    [1,'black']
                ],
            },
        },
        pointer: {
            length: '40%',
            //指针样式设置
            itemStyle:{
                color:'rgba(255,0,255,0.5)'
            },
        },

    }],
        
};
myChart.setOption(option1);