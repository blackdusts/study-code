//基础雷达图
var myChart = echarts.init(document.getElementById("t2"));
var option1 = {
    title: {
        text: '课程考试情况综合对比(雷达图)',
        bottom: 'bottom',
        left: 'center',
    },
    legend: {
        left: 'right',
        orient: 'vertical',
    },
    //设置比较维度
    radar: {
        indicator: [
            {name:'语文',max:150},
            {name:'数学',max:150},
            {name:'理综',max:300},
            {name:'体育',max:100},
            {name:'艺术',max:100}
        ],
        //修改外圈显示的形状
        shape: 'circle',
        //指示器轴分割的段数
        splitNumber: 8,
        splitArea: {
            show: true,
            areaStyle: {
                color: ['rgba(0,250,250,0.3)','rgba(250,0,250,0.3)'],
            },
        },
    },
    visualMap: [
        {
        color:['blue','red','green'],
        calculable:true,
        orient:'horizontal',
        top:'top',
        left:'left',
    }
    ],
    tooltip: {
        show: true,
    },
    series: [{
        type:'radar',
        //设置比较数据
        data:[{
            value: [101,120,210,70,60],
            name: '张三',
        },{
            value: [122,130,160,60,90],
            name: '老王',
        },{
            value: [50,30,60,60,30],
            name: '小明',
        }],
    }],
        
};
myChart.setOption(option1);