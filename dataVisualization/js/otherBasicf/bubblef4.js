//具有涟漪效果的气泡图
function getRndInteger(min, max) {
    return Math.floor(Math.random() * (max - min) ) + min;
}

var myChart = echarts.init(document.getElementById("t4"));

function dataChange(){ 
    //先生成模拟的数据 
    var data=[];
    var x,y,z;
    for(var i=0;i<50;i++){
        x=getRndInteger(1,100);
        y=getRndInteger(1,100);
        z=getRndInteger(2,30);
        data.push([x,y,z]);
    }
    
    var option1 = {
        xAxis: {},
        yAxis: {},
        title: {
            text: '动态气泡图',
            left: 'center',
        },
        series: [{
            type:'scatter',
            data:data,
            symbolSize: function(valuex){
                return valuex[2];
            },
        }],
            
    };
    //更新数据
    myChart.setOption(option1);
}

//画出第一个初始化的图
dataChange();
//之后定期更新
setInterval(function(){dataChange();},3000);