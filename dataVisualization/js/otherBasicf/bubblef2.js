//具有涟漪效果的气泡图
var myChart = echarts.init(document.getElementById("t2"));
var option1 = {
    xAxis: {},
    yAxis: {},
    title: {
        text: '具有涟漪效果的气泡图',
        left: 'center',
    },
    series: [{
        type:'effectScatter',
        data:[[1,1,20],[2,3,39],[5,2,13],[7,6,27]],
        symbolSize: function(valuex){
            return valuex[2];
        },
    }],
        
};
myChart.setOption(option1);