//基础雷达图
var myChart = echarts.init(document.getElementById("t2"));
var option1 = {
    title: {
        text: '马斯洛需求层次(漏斗图)',
        left: 'center',
    },
    series: [{
        type:'funnel',
        sort: 'ascending',
        //标签调整
        label: {
            show: true,
            backgroundColor: 'black',
            fontSize: 15,
            fontStyle: 'oblique',
            position: 'left',
        },
        //改成左三角
        funnelAlign: 'left',
        //设置位置
        width: '80%',
        left: 'right',
        bottom: '5%',
        top: '10%',
        data:[{
            value: 5,
            name: '生理需求',
        },{
            value: 4,
            name: '安全需求',
        },{
            value: 3,
            name: '爱与归属',
        },{
            value: 2,
            name: '尊重需求',
        },{
            value: 1,
            name: '自我实现',
        }],
    }],
        
};
myChart.setOption(option1);