$.get("data/work.csv",function(data){
    // console.log(data);
    ndata = data.split('\r\n');
    // console.log(ndata);
    var newData = [];
    for(var i=0;i<ndata.length;i++){
        newData.push(ndata[i].split(','));
    }
    // console.log(newData[0]);
    // console.log(newData[1]);
    var value_show=[];
    var name_company=[];
    for(var i=1;i<newData.length;i++){
        value_show.push(newData[i][2]);
        name_company.push(newData[i][0]);
    }
    var myChart = echarts.init(document.getElementById("t2"));
    //画图配置
    option = {
        xAxis: {
            type: 'category',
            data: name_company,
            name: newData[0][0],        
        },
        yAxis: {
            name: newData[0][2],
        },
        series: [
            {
                name: '待遇',
                type: 'line',
                data: value_show,
            },
        ]
    };
    // //使用刚配置的数据和配置项来生成图表；
    myChart.setOption(option);
})