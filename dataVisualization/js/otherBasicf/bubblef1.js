//基础气泡图
var myChart = echarts.init(document.getElementById("t1"));
var option1 = {
    xAxis: {},
    yAxis: {},
    title: {
        text: '简单气泡图',
        left: 'center',
    },
    series: [{
        type:'scatter',
        data:[[1,1,20],[2,3,39],[5,2,13],[7,6,27]],
        //设置不同点的大小
        symbolSize: function(valuex){
            return valuex[2];
        },
        //显示
        label: {
            show: true,
            //显示的格式和位置控制
            formatter: '({@0},{@1}):{@2}',
            position: 'top',
        },
    }],
        
};
myChart.setOption(option1);