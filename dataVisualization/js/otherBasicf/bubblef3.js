//具有涟漪效果的气泡图
function getRndInteger(min, max) {
    return Math.floor(Math.random() * (max - min) ) + min;
}
var data=[];
var x,y,z;
for(var i=0;i<50;i++){
    x=getRndInteger(1,100);
    y=getRndInteger(1,100);
    z=getRndInteger(2,30);
    data.push([x,y,z]);
}
var myChart = echarts.init(document.getElementById("t3"));
var option1 = {
    xAxis: {},
    yAxis: {},
    title: {
        text: '随机生成气泡',
        left: 'center',
    },
    series: [{
        type:'scatter',
        data:data,
        symbolSize: function(valuex){
            return valuex[2];
        },
    }],
        
};
myChart.setOption(option1);