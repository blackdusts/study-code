//最简单的饼图
var myChart1 = echarts.init(document.getElementById("t1"));
var option1 = {
    baseOption:{
        timeline:{
            axisType:'category',
            autoPlay:'true',
            //变化间隔
            playInterval: 1000,
            //三组数据
            data:['2020','2021','2022'],
        },
        title:{
            text:'成绩变化',
            left:'center',
        },
        xAxis:{
            type:'category',
            data:["小明","小美","老王"],
        },
        yAxis:{
            name:'成绩',
            min:60,
            max:100,
        },
    },
    options:[{
        series:[{
            type:'bar',
            data: [75,68,83],
        }]
    },{
        series:[{
            type:'bar',
            data: [72,70,88],
        }]
    },{
        series:[{
            type:'bar',
            data: [83,72,90],
        }]
    }],
        
};
myChart1.setOption(option1);