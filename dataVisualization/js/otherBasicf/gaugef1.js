//基础仪表盘
var myChart = echarts.init(document.getElementById("t1"));
var option1 = {
    series: [{
        type:'gauge',
        //设置仪表盘范围
        min: 0,
        max: 200,
        data:[{
            value: 80,
            name: '当前速度',
        }],
    }],
        
};
myChart.setOption(option1);