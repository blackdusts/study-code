//基础雷达图
var myChart = echarts.init(document.getElementById("t1"));
var option1 = {
    title: {
        text: '课程考试情况综合对比(雷达图)',
    },
    //设置比较维度
    radar: {
        indicator: [
            {name:'语文',max:150},
            {name:'数学',max:150},
            {name:'理综',max:300},
            {name:'体育',max:100},
            {name:'艺术',max:100}
        ],
    },
    series: [{
        type:'radar',
        //设置比较数据
        data:[{
            value: [101,120,210,70,60],
            name: '张三',
        },{
            value: [122,130,160,60,90],
            name: '老王',
        }],
    }],
        
};
myChart.setOption(option1);