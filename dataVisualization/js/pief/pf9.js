var myChart9 = echarts.init(document.getElementById("t9"));
var startAngle = 90;
var showTime = 0;
var option9 = {   
  title: {
      text: '0',
      left: 'center',
      bottom: 'center',
  },
  series: [{
    type:'pie',
    radius: ['78%','80%'],
    // center: ['50%','50%'],
    label: {
      show: false,
    },
    itemStyle: {
      color: 'red',
    },
    data: [1],
  },{
    type:'pie',
    radius: ['60%','76%'],
    startAngle:startAngle,
    // center: ['50%','50%'],
    label: {
      show: false,
    },
    itemStyle: {
      // color: 'yellow',
      borderWidth: 5,
      borderColor: 'white',
      borderRadius: 20,
    },
    data: [1,1],
  },,{
    type:'pie',
    radius: ['50%','58%'],
    // center: ['50%','50%'],
    label: {
      show: false,
    },
    itemStyle: {
      color: 'black',
      borderWidth: 5,
      borderColor: 'white',
      borderRadius: 20,
    },
    data: [1],
  }],
};
myChart9.setOption(option9);
setInterval(function(){
  showTime = (showTime+1)%12;
  startAngle = (startAngle + 30)%360;
  if(startAngle==0){
    option9.series[1].animation=false;
  }else{
    option9.series[1].animation=true;
  }
  option9.title.text = String(showTime);
  option9.series[1].startAngle = startAngle;
  
  myChart9.setOption(option9);
},1000);