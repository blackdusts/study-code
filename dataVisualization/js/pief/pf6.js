// 设置南丁格尔图,通过半径大小区分数据的大小。
var myChart = echarts.init(document.getElementById("t6"));
var option1 = {
    title: {
        text: '成绩占比(南丁格尔图)',
        left: 'center',
    },
    legend: {
        left: 'left',
        orient: 'vertical',
        top: 'middle',
    },
    tooltip: {
        formatter: '{b}:{c}({d}%)',
    },
    series: [{
        type:"pie",
        roseType: true,
        // radius: ['50%','70%'],
        label:{
            position:'inside',
        },
        data: [
            {value: 1, name: '优'},
            {value: 2, name: '良'},
            {value: 3, name: '中'},
            {value: 4, name: '差'}],
    }],
        
};
myChart.setOption(option1);