// 设置详细的数据data
// 添加标题、图例组件
// 调整组件的位置
var myChart = echarts.init(document.getElementById("t2"));
var option1 = {
    title: {
        text: '成绩占比',
        subtext: '添加title、legend',
        left: 'left',
    },
    legend: {
        left: 'left',
        orient: 'vertical',
        top: 'middle',
    },
    series: [{
        type:"pie",
        data: [
            {value: 1, name: '优'},
            {value: 2, name: '良'},
            {value: 3, name: '中'},
            {value: 4, name: '差'}],
    }],
        
};
myChart.setOption(option1);