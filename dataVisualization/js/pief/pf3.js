//添加tooltip组件
var myChart = echarts.init(document.getElementById("t3"));
var option1 = {
    title: {
        text: '成绩占比',
        subtext: '添加tooltip',
        left: 'left',
    },
    legend: {
        left: 'left',
        orient: 'vertical',
        top: 'middle',
    },
    tooltip: {
        formatter: '{b}:{c}({d}%)',
    },
    series: [{
        type:"pie",
        data: [
            {value: 1, name: '优'},
            {value: 2, name: '良'},
            {value: 3, name: '中'},
            {value: 4, name: '差'}],
    }],
        
};
myChart.setOption(option1);