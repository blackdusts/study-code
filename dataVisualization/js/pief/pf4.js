//标签向内显示
var myChart = echarts.init(document.getElementById("t4"));
var option1 = {
    title: {
        text: '成绩占比(标签向内)',
        left: 'center',
    },
    legend: {
        left: 'left',
        orient: 'vertical',
        top: 'middle',
    },
    tooltip: {
        formatter: '{b}:{c}({d}%)',
    },
    series: [{
        type:"pie",
        label:{
            position:'inside',
        },
        data: [
            {value: 1, name: '优'},
            {value: 2, name: '良'},
            {value: 3, name: '中'},
            {value: 4, name: '差'}],
    }],
        
};
myChart.setOption(option1);