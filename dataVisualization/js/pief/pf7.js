var myChart7 = echarts.init(document.getElementById("t7"));
var data = [{
  value:1,
  name:'1',
}];
var option7 = {
    title:{
      text:"动态图",
      left:'center',
    },
    series: [
      {
        type: "pie",
        label:{
            position:'inside',
        },
        data: data,
      },
    ],
};
var myInterval = setInterval(function () {
    //最多200项数据，多了就重新来
    if (data.length === 100) {  
      data = [];
      //停止定时
      // clearInterval(myInterval); 
    } else {
      data.push({
          value:data.length,
          name:String(data.length),
      });
      option7.series[0].data = data;
      myChart7.setOption(option7);
    }
  }, 100);
 
  