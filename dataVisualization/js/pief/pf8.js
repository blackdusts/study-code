var myChart8 = echarts.init(document.getElementById("t8"));
var option8 = {   
  title: {
      text: '多饼图绘制',
      left: 'center',
  },
  legend: {
      data: ['优','良','中','差'],
      bottom: 'bottom',
  },
  series: [{
    type:'pie',
    roseType: 'area',
    radius: [0,'80%'],
    center: ['25%','50%'],
    name: '1班',
    label: {
      show: true,
      color: 'black',
      position: 'inside',
    },
    itemStyle: {
      borderRadius:7,
      //下面的宽度和颜色都要设置，不然无效果
      borderWidth: 3,
      borderColor: 'white',
    },
    data: [{
      value: 4,
      name: '优',
    },{
      value: 5,
      name: '良',
    },{
      value: 6,
      name: '中',
    },{
      value: 7,
      name: '差',
    }],
  },{
    type:'pie',
    radius: [0,'50%'],
    center: ['75%','50%'],
    name: '2班',
    data: [{
      value: 2,
      name: '优',
    },{
      value: 6,
      name: '良',
    },{
      value: 2,
      name: '中',
    },{
      value: 3,
      name: '差',
    }],
  }],
};
myChart8.setOption(option8);
  